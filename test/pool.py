from dataclasses import dataclass

@dataclass
class Pool:
  x: float
  y: float
  fee_percent: float

  def __init__(self, x_reserve, y_reserve, fee_percent):
      self.x = x_reserve
      self.y = y_reserve
      self.fee_percent = fee_percent


  def k(self) -> float:
    return self.x * self.y 

  def fee_factor(self) -> float:
    return 1 - self.fee_percent / 100

  def swap_x_for_y(self, x_in: float) -> float:
    fee_factor = self.fee_factor()
    x_in_with_fee = x_in * fee_factor
    y_out = self.y - (self.k() / (self.x + x_in_with_fee))
    self.x += x_in
    self.y -= y_out
    return y_out


  def swap_y_for_x(self, y_in: float) -> float:
    fee_factor = self.fee_factor()
    y_in_with_fee = y_in * fee_factor
    x_out = self.x - (self.k() / (self.y + y_in_with_fee))
    self.x -= x_out
    self.y += y_in
    return x_out


  def x_spot_price(self) -> float:
    return self.y / self.x

  def y_spot_price(self) -> float:
    return self.x / self.y
